--------------------------------------
-- Counter 
--------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--------------------------------------

entity counter is
port(	
      CLK, CLR: in std_logic;
      Q: out std_logic_vector(3 downto 0));
end entity;  

--------------------------------------

architecture rtl of counter is
  
signal Q_int:unsigned(3 downto 0); 

begin
process (CLR,CLK)
  begin
    if (CLR='0') then
      Q_int <= "0000";
    elsif rising_edge(CLK) then
      Q_int <= Q_int + 1;
    end if;
end process;

Q <= std_logic_vector(Q_int);

end rtl;

