library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity sync_6 is
    Port ( on_off : in  STD_LOGIC;
           which_led : in  STD_LOGIC;
           prog : in  STD_LOGIC;
		     clk: in STD_LOGIC;
           led_1 : out  STD_LOGIC;
           led_2 : out  STD_LOGIC);
end sync_6;

architecture Behavioral of sync_6 is

signal new_led_1: std_logic := '0';
signal new_led_2: std_logic := '0';
signal select1, select2: std_logic;
begin

 process (clk) begin

   if rising_edge(clk) then
    select1 <= not(which_led) and prog;
	 select2 <= which_led and prog;
    if (select1 = '1') then 
      new_led_1 <= on_off;
    end if;
    
    if (select2 = '1') then 
      new_led_2 <= on_off;
    end if;
  
   end if;
    
 end process; 


 led_1 <= new_led_1;
 led_2 <= new_led_2;
end Behavioral;

