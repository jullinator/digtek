library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stopwatch is
    port(
        clk, system_clock, mux_clock, mode, reset: in std_logic;
        lamp: out std_logic;
        display: out unsigned(1 downto 0);
        seg_display: out unsigned(6 downto 0)
        );
      end entity;
     
    architecture calc of stopwatch is
        signal change_screen: unsigned(1 downto 0):= "00";
        signal carry_out0, carry_out1, carry_out2: std_logic;
        signal time_enable: std_logic := '0';
        signal prev_mode: std_logic := '0';
        signal prev_clock: std_logic := '0';
        signal prev_mux: std_logic := '0';
        signal prev_reset: std_logic := '0';

        signal minute_x_10: unsigned(3 downto 0):= "0000";
        signal minute_x_1: unsigned(3 downto 0):= "0000";
        signal second_x_10: unsigned(3 downto 0):= "0000";
        signal second_x_1: unsigned(3 downto 0):= "0000";
        signal screen: unsigned(3 downto 0);
         
        component Decoder
            port(d_in: in std_logic_vector(3 downto 0);
                 d_out: out std_logic_vector(6 downto 0));
        end component;

    begin
      process (system_clock)
        begin 
          if rising_edge(system_clock) then
            if (mux_clock = '1' and prev_mux = '0') then
              change_screen <= change_screen + 1;
            end if;
      
            carry_out0 <= '0';
            carry_out1 <= '0';
            carry_out2 <= '0';
            
            if (clk = '1' and prev_clock = '0' and time_enable = '1') then
                if (second_x_1 = "1001") then
                second_x_1 <= "0000";
                carry_out0 <= '1';
                else
                second_x_1 <= second_x_1 + 1;
                
                end if;
            end if;
            
            if(carry_out0 = '1') then 
                if (second_x_10 = "0101") then
                second_x_10 <= "0000";
                carry_out1 <= '1';
                
                else 
                second_x_10 <= second_x_10 + 1;
                
                end if;
            end if;
                
            if( carry_out1 = '1') then
                if(minute_x_1 = "1001") then
                carry_out2 <= '1';
                minute_x_1 <= "0000";
                
                else
                minute_x_1 <= minute_x_1 + 1;
                
                end if;
            end if;
              
            if(carry_out2 = '1') then 
              if(minute_x_10 = "0101") then
                minute_x_10 <= "0000";
                
              else 
                minute_x_10 <= minute_x_10 + 1;
                
              end if;
            end if;
            
            if( reset = '1' and prev_reset = '0') then
              minute_x_10 <= "0000";
              minute_x_1 <= "0000";
              second_x_10 <= "0000";
              second_x_1 <= "0000";
              
            end if;
            
            if(mode = '1' and prev_mode = '0') then
             time_enable <= not(time_enable);
              
            end if; 
            
            prev_mode <= mode;
            prev_clock <= clk;
            prev_mux <= mux_clock;
            prev_reset <= reset;
            
            case change_screen is
              when "00" => screen <= second_x_1;
              when "01" => screen <= second_x_10;
              when "10" => screen <= minute_x_1;
              when others => screen <= minute_x_10;
                      
            end case;
           end if; 
          end process;
          
        lamp <= time_enable;
        display <= change_screen;

        -- Component to decode the binary number to the seg_display display
        Decoder: display_number
        port map(d_in => screen, d_out => seg_display);


        end calc;                          
            
            
             
            
        
          
          
          
        
        
  
