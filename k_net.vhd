----------------------------------------------------------------------------------
-- Company:  
-- Engineer:
-- 
-- Create Date:    18:51:36 03/05/2018 
-- Design Name: 
-- Module Name:    k_net - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity k_net is
    Port ( x : in  STD_LOGIC;
           y : in  STD_LOGIC;
           z : in  STD_LOGIC;
           a : out  STD_LOGIC;
           b : out  STD_LOGIC;
           c : out  STD_LOGIC;
           d : out  STD_LOGIC);
end k_net;

architecture Behavioral of k_net is

begin
    a <= not z;
    b <= not (x and y);
    c <= x xor y;
    d <= x xor z;

end Behavioral;

