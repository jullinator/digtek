library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stopwatch is
  port(
        clk, system_clock, mux_clock, mode, reset: in std_logic;
        lamp: out std_logic;
        display: out unsigned(1 downto 0);
        segment: out unsigned(6 downto 0)
        );
      end entity;
     
  architecture calc of stopwatch is
          signal minute10: unsigned(3 downto 0):= "0000";
          signal minute1: unsigned(3 downto 0):= "0000";
          signal second10: unsigned(3 downto 0):= "0000";
          signal second1: unsigned(3 downto 0):= "0000";
          signal screen: unsigned(3 downto 0);
         
          signal change_screen: unsigned(1 downto 0):= "00";
          signal carry_out0, carry_out1, carry_out2: std_logic;
          signal time_enable: std_logic := '0';
          
          signal last_mode: std_logic := '0';
          signal last_clock: std_logic := '0';
          signal last_mux: std_logic := '0';
          signal last_reset: std_logic := '0';
          
          
    begin
      process (system_clock)
        begin 
          if rising_edge(system_clock) then
            if (mux_clock = '1' and last_mux = '0') then
              change_screen <= change_screen + 1;
            end if;
      
          carry_out0 <= '0';
          carry_out1 <= '0';
          carry_out2 <= '0';
            
          if (clk = '1' and last_clock = '0' and time_enable = '1') then
            if (second1 = "1001") then
              second1 <= "0000";
              carry_out0 <= '1';
            else
              carry_out0 <= '0';
              second1 <= second1 + 1;
              
            end if;
          end if;
            
          if(carry_out0 = '1') then 
            if (second10 = "0101") then
              second10 <= "0000";
              carry_out1 <= '1';
              
            else 
              carry_out1 <= '0';
              second10 <= second10 + 1;
              
            end if;
          end if;
            
          if( carry_out1 = '1') then
            if(minute1 = "1001") then
              carry_out2 <= '1';
              minute1 <= "0000";
              
            else
              carry_out2 <= '0';
              minute1 <= minute1 + 1;
              
            end if;
          end if;
              
            if(carry_out2 = '1') then 
              if(minute10 = "0101") then
                minute10 <= "0000";
                
              else 
                minute10 <= minute10 + 1;
                
              end if;
            end if;
            
            if( reset = '1' and last_reset = '0') then
              minute10 <= "0000";
              minute1 <= "0000";
              second10 <= "0000";
              second1 <= "0000";
              
            end if;
            
            if(mode = '1' and last_mode = '0') then
             time_enable <= not(time_enable);
              
            end if; 
            
            last_mode <= mode;
            last_clock <= clk;
            last_mux <= mux_clock;
            last_reset <= reset;
            
            case change_screen is
              when "00" => screen <= second1;
              when "01" => screen <= second10;
              when "10" => screen <= minute1;
              when others => screen <= minute10;
                      
            end case;
           end if; 
          end process;
          
         lamp <= time_enable;
         display <= change_screen;
          
         with screen select
         segment <= 
                 "0110000" when "0001",
                 "1101101" when "0010",
                 "1111001" when "0011",
                 "0110011" when "0100",
                 "1011011" when "0101",
                 "1011111" when "0110",
                 "1110000" when "0111",
                 "1111111" when "1000",
                 "1110011" when "1001",
                 "1111110" when others;
          
            
           
         end calc;                          
            
            
             
            
        
          
          
          
        
        
  
