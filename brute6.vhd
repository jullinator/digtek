----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:55:40 03/06/2018 
-- Design Name: 
-- Module Name:    brute6 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;ss

entity brute6 is
    Port ( on_off : in  STD_LOGIC;
           which_led : in  STD_LOGIC;
           prog : in  STD_LOGIC;
           led_1 : out  STD_LOGIC;
           led_2 : out  STD_LOGIC);
end brute6;

architecture Behavioral of brute6 is

signal prev_led_1, prev_led_2: std_logic;

begin
 led_1  <= (not which_led) and on_off when prog = '1'
				else prev_led_1;
 led_2  <= which_led and on_off when prog = '1'
				else prev_led_2;
 prev_led_1 <= (not which_led) and on_off;
 prev_led_2 <= which_led and on_off;
end Behavioral;



