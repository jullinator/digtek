library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stopwatch is
    port(
        clk, system_clk, mux_clk, mode, reset: in std_logic;
        led: out std_logic;
        display: out unsigned(1 downto 0);
        seg_display: out unsigned(6 downto 0)
        );
end entity;
     
architecture calc of stopwatch is
        signal minute_x_10: unsigned(3 downto 0):= "0000";
        signal minute_x_1: unsigned(3 downto 0):= "0000";
        signal second_x_10: unsigned(3 downto 0):= "0000";
        signal second_x_1: unsigned(3 downto 0):= "0000";
        signal screen: unsigned(3 downto 0);
        
        signal change_screen: unsigned(1 downto 0):= "00";
        signal carry_out0, carry_out1, carry_out2: std_logic;
        signal time_en: std_logic := '0';
        
        signal prev_clock: std_logic := '0';
        signal prev_mode: std_logic := '0';
        signal prev_reset: std_logic := '0';
        signal prev_mux: std_logic := '0';

begin
    process (system_clk)
    begin 
        if rising_edge(system_clk) then
            if (mux_clk = '1' and prev_mux = '0') then
                change_screen <= change_screen + 1;
            end if;
        
            carry_out0 <= '0';
            carry_out1 <= '0';
            carry_out2 <= '0';
            
            if (clk = '1' and prev_clock = '0' and time_en = '1') then
                if (second_x_1 = "1001") then
                    second_x_1 <= "0000";
                    carry_out0 <= '1';
                else
                    second_x_1 <= second_x_1 + 1;
                    
                end if;
            end if;
            
            if(carry_out0 = '1') then 
                if (second_x_10 = "0101") then
                    second_x_10 <= "0000";
                    carry_out1 <= '1';
                    
                else 
                    second_x_10 <= second_x_10 + 1;
                    
                end if;
            end if;
            
            if( carry_out1 = '1') then
                if(minute_x_1 = "1001") then
                    carry_out2 <= '1';
                    minute_x_1 <= "0000";
                    
                else
                    minute_x_1 <= minute_x_1 + 1;
                    
                end if;
            end if;
                
            if(carry_out2 = '1') then 
                if(minute_x_10 = "0101") then
                    minute_x_10 <= "0000";
                
                else 
                    minute_x_10 <= minute_x_10 + 1;
                
                end if;
            end if;
            
            if( reset = '1' and prev_reset = '0') then
                minute_x_10 <= "0000";
                minute_x_1 <= "0000";
                second_x_10 <= "0000";
                second_x_1 <= "0000";
                
            end if;
            
            if(mode = '1' and prev_mode = '0') then
                time_en <= not(time_en);
                
            end if; 
            
            prev_mode <= mode;
            prev_clock <= clk;
            prev_mux <= mux_clk;
            prev_reset <= reset;
            
            case change_screen is
                when "00" => screen <= second_x_1;
                when "01" => screen <= second_x_10;
                when "10" => screen <= minute_x_1;
                when others => screen <= minute_x_10;
                        
            end case;
        end if; 
    end process;
        
    led <= time_en;
    display <= change_screen;
    
    with screen select
    seg_display <= 
            "0110000" when "0001",
            "1101101" when "0010",
            "1111001" when "0011",
            "0110011" when "0100",
            "1011011" when "0101",
            "1011111" when "0110",
            "1110000" when "0111",
            "1111111" when "1000",
            "1110011" when "1001",
            "1111110" when others;   
end calc;                          
